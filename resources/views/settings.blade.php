@extends('shopify-app::layouts.default')

@section('styles')
    @parent
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
        .form-zero {
            display: none;
        }
        .form-zero.loaded {
            display: block;
        }
        .loading.loaded {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div
        x-data="contactForm()"
        style="background-color: rgb(246, 246, 247); color: rgb(32, 34, 35); min-height: 100vh; padding-top: 25px;"
    >
        <div class="container">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="setting-tab" data-bs-toggle="tab" data-bs-target="#pills-setting" type="button" role="tab" aria-controls="setting" aria-selected="true">Setting</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="help-tab" data-bs-toggle="tab" data-bs-target="#pills-help" type="button" role="tab" aria-controls="help" aria-selected="false">Help</button>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-setting" role="tabpanel" aria-labelledby="pills-setting-tab">
                    <div class="card">
                        <div class="card-body">
                            <div class="loading" :class="hasInit && 'loaded'">
                                <div class="spinner-border text-success" role="status">
                                    <span class="visually-hidden">Loading...</span>
                                </div>
                            </div>
                            <div class="form-zero" :class="hasInit && 'loaded'">
                                <div class="alert alert-success" role="alert">
                                    <p>As we're using the Shopify theme app extension feature, you'll have to enable or disable it from the theme customizer to see the impacts on the storefront.</p>
                                    <p>
                                        <button class="btn btn-primary btn-sm" onclick="goEdit()">Enable / Disable App</button>
                                        <button class="btn btn-secondary btn-sm" onclick="(new bootstrap.Tab(document.querySelector('#help-tab'))).show()">Learn more</button>
                                    </p>
                                </div>

                                <p x-text="message"></p>
                                <form @submit.prevent="submitData">
                                    <div class="mb-3">
                                        <label for="position" class="form-label">Snow Color</label>
                                        <div class="col-3">
                                            <input class="form-control" type="color" x-model="meta_value.snow_color">
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="position" class="form-label">Snow Speed</label>
                                        <div class="col-3">
                                            <input class="form-control" type="number" x-model="meta_value.animation_interval">
                                        </div>
                                        <p class="form-text">Theoretical "milliseconds per frame" measurement. 20 = fast + smooth, but high CPU use. 50 = more conservative, but slower</p>
                                    </div>
                                    <div class="mb-3">
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault" x-model="meta_value.snow_stick">
                                            <label class="form-check-label" for="flexSwitchCheckDefault">Snow Stick</label>
                                        </div>
                                        <p class="form-text">Allows the snow to "stick" to the bottom of the window. When off, snow will never sit at the bottom.</p>
                                    </div>
                                    <button type="submit" class="btn btn-primary" x-text="buttonLabel" :disabled="loading">Save</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-help" role="tabpanel" aria-labelledby="pills-help-tab">
                    <div class="card">
                        <div class="card-body">
                            <div class="alert alert-success" role="alert">
                                <p>As we're using the Shopify theme app extension feature, you'll have to enable or disable it from the theme customizer to see the impacts on the storefront.</p>
                                <p>
                                    <button class="btn btn-primary btn-sm" onclick="goEdit()">Enable / Disable App</button>
                                    <button class="btn btn-secondary btn-sm" onclick="(new bootstrap.Tab(document.querySelector('#setting-tab'))).show()">Setting</button>
                                </p>
                            </div>
                            <h3>Setting up the app</h3>
                            <h4>Step 1.</h4>
                            <p>Change the icon style you want to apply to the store.</p>
                            <h4>Step 2.</h4>
                            <p>Save the setting by clicking on the 'Save' button.</p>
                            <h4>Step 3.</h4>
                            As we're using the Shopify theme app extension feature, you'll have to enable or disable it from the theme customizer to see the impacts on the storefront. <br />
                            Click on the <strong>"IMZ Scroll To Top"</strong> to enable this app from theme customizer.
                            <h4>Step 4.</h4>
                            <p>After that click on the "Save button"</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        actions.TitleBar.create(app, {title: 'Dashboard'});
    </script>
    <script>
        function goEdit() {
            const redirect = actions.Redirect.create(app);
            redirect.dispatch(
                actions.Redirect.Action.ADMIN_PATH,
                "/themes/current/editor?context=apps&template=index"
            )
        }
        function contactForm() {
            return {
                hasInit: false,
                loading: true,
                buttonLabel: 'Save',
                is_active: false,
                meta_value: {},
                message: '',
                getInit(loop = 1) {
                    if (loop >= 3) {
                        this.loading = false;
                        return false;
                    }
                    this.loading = true;
                    this.buttonLabel = 'Loading'
                    window.axios.get('/settings').then((res) => {
                        const data = res.data;
                        this.is_active = data.is_active;
                        this.meta_value = data.meta_value;
                        this.message = '';
                        this.hasInit = true;
                        this.loading = false;
                        this.buttonLabel = 'Save'
                    }).catch(() => {
                        setInterval(() => {
                            loop += 1;
                            this.getInit(loop)
                        }, 5000)
                    })
                },
                init() {
                    window.onload = () => {
                        this.getInit(1)
                    }
                },
                submitData() {
                    this.buttonLabel = 'Saving...'
                    this.loading = true;
                    window.axios.post('/settings', {
                        is_active: this.is_active,
                        meta_value: this.meta_value,
                    }).then(() => {
                        this.message = ''
                        let Toast = actions.Toast;
                        const toastNotice = Toast.create(app, {
                            message: 'Setting Saved',
                            duration: 5000,
                        });
                        toastNotice.dispatch(Toast.Action.SHOW);
                    }).catch(() => {
                        this.message = ''
                        let Toast = actions.Toast;
                        const toastNotice = Toast.create(app, {
                            message: 'Ooops! Something went wrong!',
                            duration: 5000,
                            isError: true,
                        });
                        toastNotice.dispatch(Toast.Action.SHOW);
                    }).finally(() => {
                        this.loading = false;
                        this.buttonLabel = 'Save'
                    })
                }
            }
        }
    </script>
@endsection
