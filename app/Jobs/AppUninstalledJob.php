<?php

namespace App\Jobs;

use App\Models\User;
use Osiset\ShopifyApp\Actions\CancelCurrentPlan;
use Osiset\ShopifyApp\Contracts\Commands\Shop as IShopCommand;
use Osiset\ShopifyApp\Contracts\Queries\Shop as IShopQuery;

class AppUninstalledJob extends \Osiset\ShopifyApp\Messaging\Jobs\AppUninstalledJob
{
    public function handle(IShopCommand $shopCommand, IShopQuery $shopQuery, CancelCurrentPlan $cancelCurrentPlanAction): bool
    {
        parent::handle($shopCommand, $shopQuery, $cancelCurrentPlanAction);
        // Get the shop
        $user = User::withTrashed()->where('name', $this->domain->toNative())->first();
        if (!empty($user)) {
            $user->shopify_freemium = true;
            $user->save();
        }

        return true;
    }
}
