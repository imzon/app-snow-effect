<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function getSetting(Request $request)
    {
        $user = auth()->user();
        $setting = $user->setting;
        if (empty($setting)) {
            $default = [
                'snow_color' => 'bottom-right',
                'animation_interval' => '#FF9800',
                'snow_stick' => 1,
            ];
            $request = $user->api()->rest('POST', '/admin/metafields.json', [
                "metafield" => [
                    "namespace" => 'mapp_snow',
                    "key" => "settings",
                    "value_type" => "json_string",
                    "value" => json_encode($default),
                ]
            ]);
            $setting = $user->setting()->create([
                'is_active' => false,
                'meta_key' => 'setting',
                'meta_value' => $default,
                'script_id' => $request['body']['container']['metafield']['id'],
            ]);
        }
        return response()->json($setting);
    }

    public function saveSetting(Request $request)
    {
        $user = auth()->user();
        $setting = $user->setting;
        $setting->fill($request->all());
        $setting->save();
        $user->api()->rest('PUT', '/admin/metafields/' . $setting->script_id . '.json', [
            "metafield" => [
                "namespace" => 'mapp_snow',
                "key" => "settings",
                "value_type" => "json_string",
                "value" => json_encode($setting->meta_value),
            ]
        ]);

        return response()->json(['success' => true], 201);
    }
}
